import React from 'react'
import Image from 'next/image'

export default function Header(){
    return(
        <div>
            <div className="main_inner_search_block p-4">
            <div style={{display:'flex', justifyContent:'center',alignItems:'baseline'}}>
                <a href="/">
                <Image src="/images/yèrè2babi.png" height="80" width="100"/>
                </a>
                <h4 style={{color:'#03843d',fontFamily: 'MonteCarlo, cursive', fontSize:'2.3rem', fontweight:'bold'}}>Trouver les vrais coins de babi</h4>
            </div>
            <br/>
            <br/>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="main_input_search_part">
                                <div className="main_input_search_part_item">
                                    <input type="text" placeholder="Que rechercher-vous ?" />
                                </div>
                                <button className="button">Recherche</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}