import React from 'react';
import InfoBox from "react-google-maps/lib/components/addons/InfoBox";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";


const axios = require('axios');

const data = axios.get("http://51.68.44.231:1998/api/v1/boutiques")

console.log(data)


const defaultCenter = { lat: 5.3467, lng: -3.9992 };
const defaultMarkes = { lat: 5.3425, lng: -3.9875 };

const defaultOptions = { scrollwheel: true };

const RegularMap = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={14}
      defaultCenter={ defaultCenter }
      defaultOptions={ defaultOptions }
    >
      
      <Marker position={ defaultMarkes } />
      {/* <Marker position={ defaultMarkes1 } /> */}
    </GoogleMap>
  ))
);

const loadingElementStyle = { height: '100%' };
const containerElementStyle = { height: '100%' };
const mapElementStyle = { height: '100%' };

export default function GoogleMaps(){
  return (
    <RegularMap
      googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPVJYAOLjtOopW2wXeIFP8EZ7wMmT2PhE"
      loadingElement={<div style={ loadingElementStyle } />}
      containerElement={<div style={ containerElementStyle } />}
      mapElement={<div style={ mapElementStyle } />}
    />
  );
}
// AIzaSyCniQrgGfMXlYjX_cQ0zaSfU2KvbJ5Y2EI
