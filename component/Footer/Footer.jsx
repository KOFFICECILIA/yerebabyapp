import React from 'react'

export default function Footer(){
    return(
        <div>
            <footer className="footer p-4 text-center">
                <a href="/">
                <img src="/images/yèrè2babi.png" style={{height:'50px', borderRadius:'4px', marginBottom:'5px'}} alt='logo'/>
                </a>
                <h6>yèrè2babi : choisissez où manger, quoi voir et comment vous amuser à Abidjan.</h6>
                <p>©2021 yèrè2babi</p>
                <p> <a href="/A_propo" style={{color:'#ff7600'}}>A Propos</a> / <a href="/Avis" style={{color:'#ff7600'}}>Avis Clients</a></p>
            </footer>
        </div>
    )
}