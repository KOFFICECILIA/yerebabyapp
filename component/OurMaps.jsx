import { useState } from "react";
import ReactMapGL, { Marker } from "react-map-gl";


export default function Map({ locations }) {
  const [viewport, setViewport] = useState({
  width: "100%",
  height: "100%",
  latitude: 5.8602906105326, 
  longitude: -4.00835813468375,
  zoom: 10
});
return <ReactMapGL
  mapStyle="mapbox://styles/mapbox/streets-v11"
  mapboxApiAccessToken={process.env.MAPBOX_KEY}
  {...viewport}
  onViewportChange={(nextViewport) => setViewport(nextViewport)}
  >
    
</ReactMapGL>
}