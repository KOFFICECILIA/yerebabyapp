import React from 'react'
import Footer from '../Footer/Footer'
import Header from '../Header/Header'
import MapsBox from '../MapsBox/MapsBox'

export default function Layout({children}) {
    return(
        <>
            <div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 p-0 mapBox_left">
                            <Header />
                            {children}
                            <Footer/>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8 p-0">
                            <MapsBox/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}