import Link from 'next/link'
import styles from '../styles/Home.module.css'
import MapsBox from '../component/MapsBox/MapsBox'
import Image from 'next/image'


export default function A_propo() {
  return (
    <div style={{padding:'20px 4px'}}  className="p-4">
        <div>
            <h2 style={{fontWeight:'bold', borderLeft:'6px solid #ff7600', paddingLeft:"7px"}}>A propos de <span style={{color:'#038a43'}}>yèrè2babi</span> </h2> <br/>
            <p>lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation aliquip ex ea commodo consequat. Duis aute irure dolorin reprehenderits vol dolore fugiat nulla pariatur excepteur sint occaecat cupidatat.</p>
            <p>lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation aliquip ex ea commodo consequat. Duis aute irure dolorin reprehenderits vol dolore fugiat nulla pariatur excepteur sint occaecat cupidatat.</p>
        </div>
        <div className="mapBox" id="mapmobile">
            <MapsBox/>
        </div> <br/>
    </div>
  )
}

