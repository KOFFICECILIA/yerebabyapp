import React from 'react';
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import MapsBox from '../component/MapsBox/MapsBox'
import Image from 'next/image'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '80%',
    },
  },
}));


export default function A_propo() {
    const classes = useStyles();
    const [value, setValue] = React.useState('Controlled');

    const handleChange = (event) => {
        setValue(event.target.value);
    };
  return (
    <div style={{padding:'10px 4px'}}  className="p-4">
        <h3 style={{fontWeight:'bold', borderLeft:'6px solid #ff7600', paddingLeft:"7px"}}>Commentaires sur <span style={{color:'#038a43'}}>yèrè2babi</span> </h3> <br/>
        <div class="text-center" style={{marginBottom:'30px'}}>
        <form className={classes.root} noValidate autoComplete="off">
           
            <TextField id="outlined-basic" label="Nom et Prénoms" variant="outlined" />
            <TextField id="outlined-basic" label="Email" variant="outlined" />
            <TextField
            id="outlined-textarea"
            label="Votre Commentaire"
            placeholder="Placeholder"
            multiline
            variant="outlined"
            />
            <button className="button">Envoyer</button>
        </form>
        </div>

        <h2 style={{fontWeight:'bold', borderLeft:'6px solid #ff7600', paddingLeft:"7px"}}>Liste de Commentaires</h2>
        <div className="mapBox" id="mapmobile">
            <MapsBox/>
        </div> <br/>
    </div>
  )
}
