import Link from 'next/link'
import React from 'react'
import MapIcon from '@material-ui/icons/Map';
import MapsBox from '../../component/MapsBox/MapsBox'
import Image from 'next/image'
const link = '/Boutiques/Detail/'

export const getStaticPaths = async () => {
    const res = await fetch('http://51.68.44.231:1998/api/v1/domaines/')
    const data = await res.json()

    const paths = data.map(ninja => {
        return {
            params: {id: ninja.id.toString()}
        }
    })

    return {
        paths,
        fallback: false
    }
}

export const getStaticProps = async ({params}) => {
    const res = await fetch('http://51.68.44.231:1998/api/v1/boutiques/' + params.id)
    const data = await res.json()
    console.log(data.length)
    return {
        props: {ninja:data}
    }
}

const Details = ({ ninja }) => {
    if (ninja.length == 0) {
      return(
        <div className="p-4" style={{minHeight:"250px"}}>
          <p style={{paddingLeft:'10%'}}>Il n'existe pas de boutiques dans cette categorie pour le moment. Veuillez visiter les autres categories.</p>
          <br/> <br/> <a href="/" style={{margin:'5px 20px', padding:'10px', border:'1px solid #65b619' }}>Retour</a>
        </div>
      );
    } else {
      return(
        <div style={{paddingTop:'7px'}}>
            <div style={{padding:'4px 8px'}}>
              <ul>
                <li>
                  <Link href={link + ninja[0].id} className="boutCard">
                    <a>
                      <p className="boutTitle">{ninja[0].nom} <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginLeft:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/></p>
                        <div className="descript">
                          <p>{ninja[0].description}</p>
                          <div className="descript-img">
                           <Image src="/images/logo.png" height="70px" width="100px" alt="logo"/>
                          </div>
                        </div>
                        <h6 style={{color: '#65b619'}}> 
                          <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginRight:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/>
                          Lorem ipsum dolor sit amet, sed do eiusmod
                        </h6>
                        
                      <h5>Ouvert 24h/24 </h5>
                    </a>
                  </Link>
                </li>
              </ul>
               <br/>
                
              </div>
              <div className="mapBox" id="mapmobile">
                <MapsBox/>
              </div> <br/>
        </div>
      );
    }
      
        
}

export default Details;