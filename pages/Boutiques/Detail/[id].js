import Link from 'next/link'

import DirectionsCarIcon from '@material-ui/icons/DirectionsCar'
import PublicIcon from '@material-ui/icons/Public'
import DirectionsIcon from '@material-ui/icons/Directions'
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalk'
import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike'
import RoomIcon from '@material-ui/icons/Room'
import ServicesTabs from '../../../component/ServicesTabs'
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import EmailIcon from '@material-ui/icons/Email';
import Featured from '../../../component/Featured'
import HeureService from '../../../component/HeureService';
import MapsBox from '../../../component/MapsBox/MapsBox'

import React from 'react'
import MapIcon from '@material-ui/icons/Map';
import Image from 'next/image'

export const getStaticPaths = async () => {
    const res = await fetch('http://51.68.44.231:1998/api/v1/boutiques/5' )
    const data = await res.json()

    const paths = data.map(ninja => {
        return {
            params: {id: ninja.id.toString()}
        }
    })
    
    return {
        paths,
        fallback: false
    }
}

export const getStaticProps = async ({params}) => {
    const res = await fetch('http://51.68.44.231:1998/api/v1/boutique/' + params.id)
    const data = await res.json()
    
    return {
        props: {ninja:data}
    }
}

const Details = ({ ninja }) => {
    return (
      <div className="p-4">
      <br/> <div className="dvider-bar"></div>
      <div>
        <div className="p-2 mt-2">
          <div className="descript">
            <Image src="/images/vancouver.jpg" width='320px' height='200px' alt='photo'/>
            <div className="descript-img">
              <Image src="/images/logo.png" height="70px" width="100px" alt="logo"/>
            </div>
          </div> <br/>
          <p className='boutTitle mb-0'>{ninja.boutique[0].nom} <MapIcon style={{fontSize:'15', borderRadius:'50%', padding:'1.5px',marginLeft:'3px', color:'white', backgroundColor:'#65b619', textAlign:'center'}}/></p>
          <p className='mb-1'>Banque </p>
          <h5 style={{color:'gray'}}>Ouvert 24h/24</h5>
          <ul className='dirct'>
            <li><a href='#' className='btn btn-principal'><DirectionsIcon style={{fontSize:'20px', color:'white', textAlign:'center', margin:'3px'}}/>directions</a></li>
            <li><a href='#' className='btn btn-second p-1'><PublicIcon style={{fontSize:'40px', color:'#b67f19', textAlign:'center', }}/></a></li>
            <li><a href='#' className='btn btn-second p-1'><MapIcon style={{fontSize:'40px', color:'#b67f19', textAlign:'center',}}/></a></li>
          </ul>
          <p style={{marginTop:'10px'}}>{ninja.boutique[0].description}</p>
        </div>


        <div className="mapBox" id="mapmobile">
          <MapsBox/>
        </div> <br/>
        <div className="dvider-bar"></div>

        <div className="p-4">
            <h3 className="spons">Services</h3>
            <div className="back_gray p-2">
              <ServicesTabs/>
            </div>

            <h4 className="mt-4">Photos</h4>
            <div className="back_gray p-4" style={{height:'30rem'}}>
              <Featured/>
            </div>
          </div>
        <div className="dvider-bar"></div> <br/>
        

        <div className="p-4 boutCard">
          <h2>Contact</h2>
            <ul>
              <li className="mb-2"><RoomIcon style={{color:'#ff7600'}}/> Cocody, Abidjan, Côte divoire</li>
              <li className="mb-2"> <PhoneIphoneIcon style={{color:'#ff7600'}}/> +225 01 02 03 04 05</li>
              <li className="mb-2"> <EmailIcon style={{color:'#ff7600'}}/> boutique@gmail.com</li>
            </ul>
        </div> <br/>

        <div className="dvider-bar"></div>  <br/>
          
          <div className="destination-val">
            <div style={{width:'70%', backgroundColor:'rgba(255, 255, 255, 0.685)', margin:'auto', padding:'10px', borderRadius:'10px'}}>
              <h4>Choisissez où manger, quoi voir et comment vous amuser</h4>
            </div>
          </div>
          <br/>
        <div className="dvider-bar"></div> <br/>

        <section style={{padding:'4px', marginTop:'30px', marginBottom:'30px'}}>
          <HeureService/>
        </section>

        <div className="p-4 boutCard mb-55">
          <h3>Direction</h3>
          <div style={{padding:'20px', backgroundColor:'#f9f9f9'}} className="margin-bottom-30">
          <div className="row">
            <div className="col-3"></div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsCarIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsWalkIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsBikeIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-3"></div>
          </div> <br/>
          <div className="row">
            <div className="col-lg-1 m-auto">
              <div style={{height:'15px',border:'2px solid black', width:'15px',borderRadius:'50%', backgroundColor:'#54ba1d',margin:'auto', position: 'absolute',right:'0',bottom: '0px'}}></div>
            </div>
            <div className="col-lg-10">
                <input type="text" name="nom" placeholder="Veuillez saisir votre Position Actuelle" />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-1 m-auto">
              <div style={{height:'15px',border:'2px solid black', width:'15px',borderRadius:'50%', backgroundColor:'#f98925',margin:'auto', position: 'absolute',right:'0',bottom: '0px'}}></div>
            </div>
          <div className="col-lg-10">
              <input type="text" name="lieu" placeholder="Veuillez saisir votre Lieu choisi" />
          </div>
          </div>

          <div>
            <p style={{fontSize:'18px'}}>Temps estimé à : <span style={{color: '#54ba1d',fontWeight:'bold'}}>03heure 30minute</span></p>
          </div>

        </div>
        </div>
        
      </div>
      
    </div>
       
    );
}

export default Details