import Link from 'next/link'
import styles from '../styles/Home.module.css'
import MapsBox from '../component/MapsBox/MapsBox'
import Image from 'next/image'
const link = '/Boutiques/' 


export async function getStaticProps() {
  
  const res = await fetch('http://51.68.44.231:1998/api/v1/domaines')
  const data = await res.json()


  return {
    props: {
      data:data, 
    },
  }
}

export default function Home({data}) {
  return (
    <div style={{padding:'20px 4px', minHeight:"250px"}}  className="p-4">
      <div className="category-box">
      
        <ul>
        {data.map((domaines) => (
          
          <li key={domaines.id}>
            <div className="text-center">
                <Link href={link + domaines.id}>
                  <a>
                    <div className="cat-img">
                      <Image src={domaines.icon} height="45" width="45" alt="" />
                    </div>
                    <p>{domaines.domaine}</p>
                  </a>
                </Link>
            </div>
          </li>
    
        ))}
        </ul>
          <br/>
          
        </div>
        <div className="mapBox" id="mapmobile">
            <MapsBox/>
        </div> <br/>
        </div>
  )
}

